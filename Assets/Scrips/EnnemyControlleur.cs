﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class EnnemyControlleur : MonoBehaviour
{
    private Transform target = null;
    public float speedMovement = 1f;
    public float pushForce = 30f;
    public float pushForceProps = 30f;

    public float radiusDetection = 15f;
    public float slowMotionValue = 0.1f;
    public float durationSlowMotion = 0.1f;
    public bool explodeObsctacle = true;
    public LayerMask layerPlayer;
    public Transform rig;
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public ParticleSystem deathFX;
    public Color deathColor = Color.black;
    private new Rigidbody rigidbody;
    private Animator animator;
    private Vector3 direction;
    private Rigidbody[] rigibodyRagdoll;
    private Collider[] colliderRagdoll;
    private Collider ennemyCollider;
    private NavMeshAgent navMeshAgent;
    private float originalDeltaTime;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        ennemyCollider = GetComponent<Collider>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        rigibodyRagdoll = rig.GetComponentsInChildren<Rigidbody>();
        colliderRagdoll = rig.GetComponentsInChildren<Collider>();
        originalDeltaTime = Time.captureDeltaTime;
        
        target = VIPControlleur.Instance.targetEnnemy;
        

        //Ignore collision avec main shere 
        foreach (Rigidbody rigidbody in rigibodyRagdoll)
        {
            Physics.IgnoreCollision(ennemyCollider, rigidbody.GetComponent<Collider>(), true);
        }

        ToogleRagdoll(false);
    }

    private void ToogleRagdoll(bool state){
        foreach(Rigidbody rigidbody in rigibodyRagdoll){
            rigidbody.useGravity = state;
            rigidbody.isKinematic = !state;
        }
    }

    private void Update() {
        if(GameManager.Instance.CurrentGameState == GameState.IN_GAME){
            direction = (target.position - transform.position).normalized;
            animator.SetFloat("Speed", direction.magnitude);
            DetectPlayer();
        }
    }

    void MovementNavMeshAgent(){
        navMeshAgent.SetDestination(target.position);
    }

    public void Taunt(){
        animator.SetTrigger("Taunt");
        navMeshAgent.isStopped = true;
    }

    private void DetectPlayer(){
        bool detectPlayer = Physics.CheckSphere(transform.position, radiusDetection, layerPlayer, QueryTriggerInteraction.Ignore);
        if(detectPlayer){
            MovementNavMeshAgent();
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.tag == "Wall" || other.transform.tag == "WallPlayer"){
            if(explodeObsctacle){
                ParticleSystem explosion = Instantiate(other.gameObject.GetComponent<WallTag>().fx_explosion, other.collider.bounds.center, Quaternion.identity);
                explosion.GetComponent<ParticleSystemRenderer>().material = ThemeManager.Instance.getCurrentObsctacleMaterial();
                float size = explosion.startSize * other.transform.localScale.x;
                explosion.startSize = Mathf.Clamp(size, 0.1f, 0.15f);
                VibrationManager.VibrateHeavy();
                
                Destroy(other.gameObject);
            }

            Die(other.impulse, true);
        }
    }

    public void Die(Vector3 directionPush, bool dieFromProps){
        speedMovement = 0f;
        ennemyCollider.enabled = false;
        animator.enabled = false;
        navMeshAgent.isStopped = true;
        navMeshAgent.enabled = false;
        
        ToogleRagdoll(true);

        StartCoroutine(slowMotion(durationSlowMotion));

        // deathFX.Play();

        skinnedMeshRenderer.material.DOColor(deathColor, 1f).SetEase(Ease.OutQuad); // Main color
        skinnedMeshRenderer.material.SetColor("_OutlineColor", deathColor); // Outline color

        float pushValue = 0;
        switch(dieFromProps){
            case true:
                pushValue = pushForceProps;
            break;

            case false:
                pushValue = pushForce;
            break;
        }

        foreach (Rigidbody rigidbody in rigibodyRagdoll)
        {
            rigidbody.GetComponent<Rigidbody>().AddForce(directionPush.normalized * pushValue * Time.fixedDeltaTime, ForceMode.Impulse);
            rigidbody.gameObject.layer = LayerMask.NameToLayer("DeadEnnemy");
        }
        
        this.enabled = false;
    }

    IEnumerator slowMotion(float delay){
        Time.timeScale = slowMotionValue;
        yield return new WaitForSecondsRealtime(delay);
        Time.timeScale = 1f;
    }
}
