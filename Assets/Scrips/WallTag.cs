﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class WallTag : MonoBehaviour
{
    public float forceMultiplieur = 1f;
    public float distanceForSign = 5f;
    public Color blinkFirstColor;
    public Color blinkSecondColor;
    public float speedBlink = 1;
    public float magnitude = 0f;
    public bool deleteNavmeshObstacle = true;
    public ParticleSystem fx_explosion;

    private NavMeshObstacle navMeshObstacle;
    new private Rigidbody rigidbody;
    private new Renderer renderer;
    private Color originalFirstColor;
    private Color originalSecondColor;
    private float lerpBlinkValue = 0f;
    private bool yoyoValue = false;
    public bool isBlinking = false;


    private void Start() {
        rigidbody = GetComponent<Rigidbody>();
        navMeshObstacle = GetComponent<NavMeshObstacle>();
        if(GetComponent<Renderer>()){
            renderer = GetComponent<Renderer>();
        }else{
            renderer = null;
        }

        if(renderer != null){
            originalFirstColor = renderer.material.color;
            originalSecondColor = renderer.material.GetColor("_ColorDim");
        }
    }
    private void FixedUpdate() {
        // Collision controlle
        magnitude = rigidbody.velocity.magnitude;

        if(deleteNavmeshObstacle){

            if(rigidbody.velocity.magnitude < 1.5f){
                tag = "Wall";
                navMeshObstacle.enabled = true;
            }else{
                navMeshObstacle.enabled = false;
            }

        }
        //Sign when objet is near the VIP
        float distanceFromVIP = Vector3.Distance(transform.position, VIPControlleur.Instance.transform.position);
        
        Blink();
        
    }

    public void Blink(){
        lerpBlinkValue = Mathf.Clamp01(lerpBlinkValue);

        if(isBlinking){
            
            if(yoyoValue == false){
                lerpBlinkValue += Time.deltaTime * speedBlink;
            }else{
                lerpBlinkValue -= Time.deltaTime * speedBlink;
            }

            if(lerpBlinkValue > 1f){
                yoyoValue = true;
            }

            if(lerpBlinkValue < 0f){
                yoyoValue = false;
            }

        }else{
            lerpBlinkValue -= Time.deltaTime * speedBlink;
        }

        if(renderer != null){
            renderer.material.color = Color.Lerp(originalFirstColor, blinkFirstColor, lerpBlinkValue);
            renderer.material.SetColor("_ColorDim", Color.Lerp(originalSecondColor, blinkSecondColor, lerpBlinkValue));
        }
    }

}
