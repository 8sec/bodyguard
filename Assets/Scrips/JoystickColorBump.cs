﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickColorBump : MonoBehaviour
{
    public float deathZone = 1f;
    private float lastFrameX = 0f;
    private float lastFrameY = 0f;
    private float deltaX = 0f;
    private float deltaY = 0f;
    Vector2 deltaMove = Vector2.zero;
    Vector2 curpos = Vector2.zero;

    private void Start() {
        lastFrameX = Input.mousePosition.x;
        lastFrameY = Input.mousePosition.y;
    }

    private void Update() {
        if(Input.GetMouseButtonDown(0)){
            lastFrameX = 0f;
            lastFrameY = 0f;
            deltaX = 0f;
            deltaY = 0f;
            deltaMove = Vector2.zero;
        }

        if(Input.GetMouseButtonUp(0)){
            lastFrameX = 0f;
            lastFrameY = 0f;
            deltaX = 0f;
            deltaY = 0f;
            deltaMove = Vector2.zero;
        }


    }

    public Vector2 GetDirection(){  
        if(Input.GetMouseButton(0)){
            if(lastFrameX == 0f){
                lastFrameX = Input.mousePosition.x;
                lastFrameY = Input.mousePosition.y;
                curpos = Input.mousePosition;
                deltaX = 0f;
                deltaY = 0f;
                deltaMove = Vector2.zero;
            }

            
            curpos = Input.mousePosition;

            deltaX = curpos.x - lastFrameX;
            lastFrameX = curpos.x; 

            deltaY = curpos.y - lastFrameY;
            lastFrameY = curpos.y;

            deltaMove = new Vector2(deltaX, deltaY);
            
            if(deltaMove.magnitude > deathZone){
                return deltaMove;
            }
        }
        return Vector2.zero;
    }

}
