﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MilkShake;
using DG.Tweening;

public class DetectEnnemy : MonoBehaviour
{
    public Camera mainCamera;
    public ShakePreset shakerPunchParameter;
    public ShakePreset shakerWallParameter;

    public PlayerControlleur playerControlleur;
    public ParticleSystem[] fxHit;
    public float pushWallForce = 50f;
    public float radiusDetectionNearEnnemy = 5f;
    public LayerMask layerMaskEnnemy;

    private void OnTriggerStay(Collider other) {
        if(playerControlleur.canKill && other.GetComponent<EnnemyControlleur>()){
            playerControlleur.animator.SetTrigger("Attack");
            other.GetComponent<EnnemyControlleur>().Die((other.transform.position - transform.position + Vector3.up), false);
            Instantiate(fxHit[Random.Range(0, fxHit.Length)], transform.position, transform.rotation, transform);
            mainCamera.GetComponent<Shaker>().Shake(shakerPunchParameter);
            playerControlleur.ResetCooldown();
            VibrationManager.VibrateMedium();
        }

        if(playerControlleur.canKill && other.transform.tag == "Wall"){
            other.tag = "WallPlayer";
            playerControlleur.animator.SetTrigger("Attack");
            float forceMultiplieur = other.GetComponent<WallTag>().forceMultiplieur;
            other.GetComponent<Rigidbody>().AddForce((transform.forward + (transform.up * 0.3f)).normalized * pushWallForce * forceMultiplieur * Time.fixedDeltaTime, ForceMode.Impulse);
            // other.GetComponent<Rigidbody>().AddForce((DetectNearEnnemy() - transform.position).normalized * pushWallForce * Time.fixedDeltaTime, ForceMode.Impulse);
            mainCamera.GetComponent<Shaker>().Shake(shakerWallParameter);
            Instantiate(fxHit[Random.Range(0, fxHit.Length)], transform.position, transform.rotation, transform);
            playerControlleur.ResetCooldown();
            VibrationManager.VibrateLight();
        }
    }

    private Vector3 DetectNearEnnemy(){
        Collider[] NearEnnemy = Physics.OverlapSphere(transform.position, radiusDetectionNearEnnemy, layerMaskEnnemy, QueryTriggerInteraction.Ignore);
        Vector3 positionNearestEnnemy = Vector3.positiveInfinity;
        float nearestDistance = float.MaxValue;
        foreach (Collider ennemy in NearEnnemy)
        {
            if(Vector3.Distance(ennemy.transform.position, transform.position) < nearestDistance){
                nearestDistance = Vector3.Distance(ennemy.transform.position, transform.position);
                positionNearestEnnemy = ennemy.transform.position;
            }
        }

        if(nearestDistance != float.MaxValue){
            return positionNearestEnnemy;
        }else{
            return transform.forward;
        }
    }
}
