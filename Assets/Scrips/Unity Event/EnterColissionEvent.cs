﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnterColissionEvent : MonoBehaviour
{
    public UnityEvent onCollisionEnter;

    private void OnCollisionEnter(Collision other) {
        onCollisionEnter.Invoke();
    } 
}
