﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterEvent : MonoBehaviour
{
    public string detectionTag;
    public bool triggerOnlyOnce = false;
    public UnityEvent onTriggerEnter;

    private void OnTriggerEnter(Collider other) {
        if(detectionTag != string.Empty && other.gameObject.tag == detectionTag){
            onTriggerEnter.Invoke();
            if(triggerOnlyOnce){
                Destroy(this);
            }
        }

        if(detectionTag == string.Empty){
            onTriggerEnter.Invoke();
            if(triggerOnlyOnce){
                Destroy(this);
            }
        }
    }
}
