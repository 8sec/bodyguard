﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class PlayerControlleur : MonoBehaviourSingleton<PlayerControlleur>
{
    public JoystickColorBump joystickColorBump;
    public float speedRotation = 1f;
    public float speedMoveDeltaX = 0.55f;
    public float distanceDelta = 0.4f;
    [Range(0.1f, 1f)] public float easeMovementValue = 0.2f; 
    public Transform targetPlayer;
    private Rigidbody targetPlayerRigibody;
    public bool canKill = true;
    public float cooldownBetweenKill = 1f;
    private float tmp_cooldownBetweenKill = 0f;
    public float minRunAnimationSpeed = 0.2f;
    public float maxDistanceFromTheVIP = 10f;
    private new Rigidbody rigidbody;
    public Animator animator;
    private Vector3 direction = Vector3.zero;

    [Title("Limit Movement")]
    public float limitX = 4f;
    public float limitZNear;


    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        targetPlayerRigibody = targetPlayer.GetComponent<Rigidbody>();
        MonetizationManager.Instance.ShowInterstitial();
    }

    private void Update() {
        
        if(GameManager.Instance.CurrentGameState == GameState.IN_GAME){
            animator.SetFloat("Speed", Mathf.Clamp(direction.magnitude, minRunAnimationSpeed, 1f));
            
            // Manage Cooldown
            tmp_cooldownBetweenKill += Time.fixedDeltaTime;

            if(tmp_cooldownBetweenKill > cooldownBetweenKill){
                canKill = true;
            }
        }
    }

    void FixedUpdate()
    {
        if(GameManager.Instance.CurrentGameState == GameState.IN_GAME){
            direction = joystickColorBump.GetDirection(); // Direction new joystick
            MoveTargetPlayer(direction);
            MovePlayer();
        }
    }

    public void MoveTargetPlayer(Vector2 deltaMove){
        Vector3 delta = new Vector3(deltaMove.x, 0, deltaMove.y);
        delta = new Vector3(delta.x * speedMoveDeltaX, 0, delta.z * speedMoveDeltaX);
        targetPlayer.transform.position += delta * Time.deltaTime;

        targetPlayer.transform.position = new Vector3(Mathf.Clamp(targetPlayer.position.x, - limitX, limitX), targetPlayer.position.y, Mathf.Clamp(targetPlayer.position.z, VIPControlleur.Instance.transform.position.z - limitZNear ,  VIPControlleur.Instance.transform.position.z + maxDistanceFromTheVIP));
    }

    public void MovePlayer(){
        Vector3 directionFoward = new Vector3(direction.x, 0, direction.y).normalized;
        float distancemultiplayer = Mathf.Clamp(Vector3.Distance(transform.position, targetPlayer.position), 1f, 2f);

        // Rotation
        if(direction != Vector3.zero){
            Quaternion rotation = Quaternion.LookRotation(directionFoward, Vector3.up);
            Quaternion smoothRotation = Quaternion.Slerp(rigidbody.rotation, rotation, speedRotation * Time.deltaTime);
            rigidbody.MoveRotation(smoothRotation);
        }

        // Movement
        // Vector3 movementPlayer = transform.position + (targetPlayer.position.SetY(transform.position.y) - transform.position) * speedMovement * Time.fixedDeltaTime;
        Vector3 movementPlayer =  Vector3.MoveTowards(transform.position, Vector3.Lerp(transform.position, targetPlayer.position.SetY(transform.position.y), easeMovementValue), distanceDelta);
        Vector3 movementVIP = VIPControlleur.Instance.GetMovementVIP();

        float distanceFromVIP = Vector3.Distance(transform.position, VIPControlleur.Instance.transform.position);


        if(directionFoward.z >= 0f){
            rigidbody.MovePosition((movementPlayer + movementVIP)); // Add movement of player + VIP when going up
        }

        if(directionFoward.z < -0f){
            rigidbody.MovePosition(movementPlayer); // Add movement of player only when doing down
        }

        targetPlayer.Translate(VIPControlleur.Instance.GetMovementVIP(), Space.World);
        
    }


    public void ResetCooldown(){
        tmp_cooldownBetweenKill = 0f;
        canKill = false;
    }

    public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
     {
         Vector3 AB = b - a;
         Vector3 AV = value - a;
         return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
     }
}
