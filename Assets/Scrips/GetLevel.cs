﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GetLevel : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = "Level " + (LevelManager.Instance.CurrentLevelIndex);
    }

}
