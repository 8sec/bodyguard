﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    void Awake()
    {
        LevelManager.Instance.StartNextLevel();
    }
}
