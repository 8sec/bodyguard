﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeManager : MonoBehaviourSingleton<ThemeManager>
{
    public List<GameObject> themeObject = new List<GameObject>();
    public List<Material> materialsObstacle = new List<Material>();
    public List<Color> colorFogs = new List<Color>();
    private Material currentMaterialObscacle;

    private void Awake() {
        int currentLevel = PlayerPrefs.GetInt("LM_CurrentLevel", 0);
        ChangeTheme(currentLevel);
    }

    private GameObject currentTheme;
    public void ChangeTheme(int themeIndex){
        int index = themeIndex % themeObject.Count;
        currentTheme = themeObject[index];

        for (int i = 0; i < themeObject.Count; i++)
        {
            if(i == index){
                //Activate 3D background
                themeObject[i].SetActive(true);

                //Change obstacle mat
                WallTag[] obsctacles = GameObject.FindObjectsOfType<WallTag>();
                currentMaterialObscacle = materialsObstacle[i];
                foreach (WallTag wall in obsctacles)
                {
                    wall.GetComponent<Renderer>().material = materialsObstacle[i];
                }

                //Change Fog color
                RenderSettings.fog = true;
                RenderSettings.fogMode = FogMode.ExponentialSquared;
                RenderSettings.fogDensity = 0.015f;
                RenderSettings.fogColor = colorFogs[i];

            }else{
                themeObject[i].SetActive(false);
            }
        }
    }

    public GameObject GetCurrentTheme(){
        return currentTheme;
    }

    public Material getCurrentObsctacleMaterial(){
        return currentMaterialObscacle;
    }
}
