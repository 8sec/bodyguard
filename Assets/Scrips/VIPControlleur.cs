﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VIPControlleur : MonoBehaviourSingleton<VIPControlleur>
{
    private new Rigidbody rigidbody;
    private Animator animator;
    public float speedMovement = 1f;
    public Transform targetEnnemy;
    public float distanceDetectionObstacle = 5f;
    public LayerMask layerMaskDetectionObstacle;
    public ParticleSystem looseFX;
    public bool win = false;
    private Vector3 direction;
    private RaycastHit lastRaycastHit;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        direction = Vector3.forward;
        UIController.Instance.ShowPanel(UIPanelName.TITLE_SCREEN); // Show Title Screen
    }

    public Vector3 GetMovementVIP(){
        return direction * speedMovement * Time.fixedDeltaTime;
    }

    private void Update() {
        // Title Screen
        if(GameManager.Instance.CurrentGameState == GameState.INITIAL_STATE){
            if(Input.GetMouseButtonDown(0)){
                GameManager.Instance.StartSession();
                PlayerControlleur.Instance.animator.SetTrigger("Start");
                animator.SetTrigger("Start");
            }
        }

        // Win Screen
        if(GameManager.Instance.CurrentGameState == GameState.LEVEL_SUCCESS){
            if(Input.GetMouseButtonDown(0)){
                LevelManager.Instance.StartNextLevel();
            }
        }

        // Loose Screen or Ask Revive
        if(GameManager.Instance.CurrentGameState == GameState.GAME_OVER || GameManager.Instance.CurrentGameState == GameState.ASK_REVIVE){
            if(Input.GetMouseButtonDown(0)){
                LevelManager.Instance.StartNextLevel();
            }
        }

    }

    private void FixedUpdate() {
        if(GameManager.Instance.CurrentGameState == GameState.IN_GAME){
            direction = Vector3.forward;
            DetectIncomingObject();
            rigidbody.MovePosition(transform.position + direction * speedMovement * Time.fixedDeltaTime);
        }
    }

    private void DetectIncomingObject(){
        RaycastHit raycastHit;
        bool touch = Physics.Raycast(transform.position, Vector3.forward, out raycastHit ,distanceDetectionObstacle, layerMaskDetectionObstacle, QueryTriggerInteraction.Ignore);

        if(touch){
            raycastHit.transform.GetComponent<WallTag>().isBlinking = true;

            if(lastRaycastHit.transform != null && lastRaycastHit.transform != raycastHit.transform){
                lastRaycastHit.transform.GetComponent<WallTag>().isBlinking = false;
            }
            lastRaycastHit = raycastHit;       
        }else if(lastRaycastHit.transform != null){
                lastRaycastHit.transform.GetComponent<WallTag>().isBlinking = false;
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.GetComponent<EnnemyControlleur>()){
            
            Loose();
        }

        if(other.transform.tag == "Wall"){
            ParticleSystem explosion = Instantiate(other.gameObject.GetComponent<WallTag>().fx_explosion, other.collider.bounds.center, Quaternion.identity);
            explosion.GetComponent<ParticleSystemRenderer>().material = ThemeManager.Instance.getCurrentObsctacleMaterial();
            float size = explosion.startSize * other.transform.localScale.x;
            explosion.startSize = Mathf.Clamp(size, 0.15f, 0.2f);
            explosion.startLifetime = 5f;
            
            Destroy(other.gameObject);

            Loose();

        }

    }

    public void Loose(){
        if(win == false){
            looseFX.Play();
            speedMovement = 0f;
            rigidbody.isKinematic = true;
            animator.SetTrigger("Loose");
            PlayerControlleur.Instance.animator.SetTrigger("Loose");
            GameManager.Instance.GameOver();
            VibrationManager.VibrateFailure();

            EnnemyControlleur[] allEnnemy = GameObject.FindObjectsOfType<EnnemyControlleur>();
            foreach (EnnemyControlleur ennemy in allEnnemy)
            {
                ennemy.Taunt();
            }
        }
    }

    public void Win(){
        win = true;
        GameManager.Instance.LevelSuccess();
        speedMovement = 0f;
        animator.SetTrigger("Win");
        PlayerControlleur.Instance.animator.SetTrigger("Win");
        VibrationManager.VibrateSuccess();
    }
}
