﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class BUTTON_ShowShop : MonoBehaviour
{

    [SerializeField] private bool _animateIcon;


    [ShowIf("_animateIcon"), Header("---- SPRITE SHOP ICONS ----")]
    [SerializeField] private List<Sprite> _shopIcons = new List<Sprite>();
    [SerializeField] private Image _buttonImage;
    [SerializeField] private float _moveMult;
    [SerializeField] private float _moveTime;

    private float timer;
    [SerializeField] private float _delayBeforeSpriteChange;
    private int currentSpriteIndex;

   public void Action()
    {
        if (UIController.Instance == null)
        {
            Debug.LogError("UIController instance is NULL. Please setup a UIController in the scene");
            return; 
        }

        UIController.Instance.ShowPanelInstantly(UIPanelName.SHOP);
    }

    private void Update()
    {
        if (_animateIcon)
        {
            timer += Time.deltaTime;

            if (timer >= _delayBeforeSpriteChange)
            {
                timer = 0;
                currentSpriteIndex++;

                if (currentSpriteIndex >= _shopIcons.Count)
                {
                    currentSpriteIndex = 0;
                }

                _buttonImage.sprite = _shopIcons[currentSpriteIndex];
                LeanTween.move(_buttonImage.gameObject, _buttonImage.transform.position - _buttonImage.transform.right * _moveMult, _moveTime).setLoopPingPong(1);
            }
        }
      
    }
}
