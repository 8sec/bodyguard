﻿using UnityEngine.UI;

public class RB_FreeCurrency : RewardedButton
{

    public int CoinRewardAmount = 100;


    public Text RewardAmountDisplayer;

    public override void CollectReward()
    {
        base.CollectReward();
        CurrencyManager.Instance.AddCurrency(CoinRewardAmount);
    }

    override protected void Start()
    {
        base.Start();
        RewardAmountDisplayer.text = "+" + CoinRewardAmount.ToString();
    }
}