﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem_Color : ShopItem
{
    [SerializeField] private Image _shopPreviewImage;
    [SerializeField] private Color _color;
    public Color Color => _color;

    private void Start()
    {
        _shopPreviewImage.color = _color;
    }

    public override void Action()
    {
        base.Action();
        Debug.LogError("CLICKED ON COLOR");
    }

}
