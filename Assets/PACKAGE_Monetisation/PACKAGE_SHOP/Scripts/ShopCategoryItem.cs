﻿public enum ShopCategoryItem
{
    UNDEFINED,
    HAT,
    ANIMATION,
    COLOR
}