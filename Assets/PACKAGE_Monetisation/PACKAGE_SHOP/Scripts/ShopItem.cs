﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using DG.Tweening;

public class ShopItem : MonoBehaviour
{
    [HideIf("ShouldHide")]
    public Image ButtonBackground;
    [HideIf("ShouldHide")]
    public Color SelectedColor;
    [HideIf("ShouldHide")]
    public Color DeselectedColor;

    bool ShouldHide => this is ShopItem == false;
    [SerializeField] private GameObject _lockedImage;

    public bool _isUnlockByDefault;


    private ItemCategory _parentCategory;

    protected string Key_Unlocked
    {
        get
        {
            return "SHOP_" + UniqueID;
        }
    }

    public bool IsUnlocked
    {
        get
        {
            return PlayerPrefs.GetInt(Key_Unlocked, 0) == 1 || _isUnlockByDefault;
        }
    }

   
    public string UniqueID
    {
        get
        {
            return name;
        }
    }

    private void Awake()
    {
        Init();
    }


    public virtual void Init()
    {
        if (IsUnlocked == false)
        {
            _lockedImage.SetActive(true);
        }

        else
        {
            _lockedImage.SetActive(false);
        }
    }



    public virtual void Action()
    {
        Select();
       // TryEquip();
    }

    public virtual void TryEquip()
    {
  //      if (IsUnlocked)
 //           ApplyCustomItem();
    }

    public virtual void Deselect()
    {
        ButtonBackground.color = DeselectedColor;
    }

    public virtual void Select()
    {
        if (IsUnlocked == false)
        {
            GetComponentInParent<ScrollRect>().enabled = false;
            transform.DOShakePosition(0.3f, 10, 30).OnComplete(new TweenCallback(OnComplete));
        }
        VibrationManager.VibrateLight();
        ButtonBackground.color = SelectedColor;
        _parentCategory.Select(this);
    }

   
    public virtual void Unlock()
    {
        PlayerPrefs.SetInt(Key_Unlocked, 1);
        _lockedImage.SetActive(false);
    }

    public void SetShopItemCategory(ItemCategory category)
    {
        _parentCategory = category;
    }

    private void OnComplete()
    {
        GetComponentInParent<ScrollRect>().enabled = true;
    }

}

