﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using AppsFlyerSDK;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public int CurrentLevelIndex;
    public static int numberLevelOnboarding = 6;

    private const string Key_CurrentLevel = "LM_CurrentLevel";

    // Use this for initialization
    public virtual void Awake()
    {
        Instance = this;
        CurrentLevelIndex = PlayerPrefs.GetInt(Key_CurrentLevel, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LevelUp();
        }
    }

    public void LevelUp()
    {
        CurrentLevelIndex++;
        PlayerPrefs.SetInt(Key_CurrentLevel, CurrentLevelIndex);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_levelIndex", (CurrentLevelIndex - 1).ToString());
        AppsFlyer.sendEvent("af_Progress_LevelSuccess", richEvent);
    }

    public void StartNextLevel(){
        int index = CurrentLevelIndex % SceneManager.sceneCountInBuildSettings;
        print("Level Load " + index);

        if(index == 0){
            SceneManager.LoadScene(6);
        }else{
            SceneManager.LoadScene(index);
        }
    }

    public void StartLevel(int i)
    {
        CurrentLevelIndex = i;
        SceneManager.LoadScene(CurrentLevelIndex);

    }

}