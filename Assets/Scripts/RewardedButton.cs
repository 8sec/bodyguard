﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardedButton : MonoBehaviour {

    public bool ManuallyControlDisabling = true;
    public static bool collectedDuringThisGame;

    protected virtual void Start () {
        if (MonetizationManager.Instance != null) {
            Initialize ();

        }
        // UIController.Instance.HidePanelInstantly(8);

    }
    bool inited = false;
    public void Initialize () {
        inited = true;
        //Debug.LogError("INIT " + name);
        //m_SelfGameObject = gameObject;
        if (MonetizationManager.Instance != null) {

            if (!MonetizationManager.rewardedButtons.Contains (this))
                MonetizationManager.rewardedButtons.Add (this);

            UpdateStatus ();
        }
    }
    private void OnEnable () {
        UpdateStatus ();
    }

    private void OnDestroy () {
        if (MonetizationManager.Instance != null) {
            if (MonetizationManager.rewardedButtons.Contains (this))
                MonetizationManager.rewardedButtons.Remove (this);
        }
    }

    public void UpdateStatus () {
        if (!inited)
            Initialize ();
        if (MonetizationManager.Instance != null) {

            if (MonetizationManager.Instance.isRewardedAdAvailable) {
                if (ManuallyControlDisabling == false)
                {
                    EnableButton();
                }
            } else {
                DisableButton ();
            }
        }

    }
    public void SetStatus (bool isActive) {
        if (isActive) {

            if (ManuallyControlDisabling == false)
            {
                EnableButton();
            }

        } else {
            DisableButton ();
        }
    }

    public virtual void EnableButton () {
        print("THERE");
        gameObject.SetActive (true);

    }

    public void DisableButton () {
        gameObject.SetActive (false);
    }

    public virtual void CollectReward () {
        DisableButton ();
    }

    public virtual void RewardedClosed () {

    }

    public virtual void ShowRewardedVideo (string placementId) {
        if (MonetizationManager.Instance != null) {
            MonetizationManager.Instance.ShowRewarded (placementId, this);
        } else {
            Debug.LogError ("No Monetization Manager found");
        }
    }
}