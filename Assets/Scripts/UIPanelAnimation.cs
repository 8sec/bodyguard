﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPanelAnimation : MonoBehaviour {

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    public virtual void ShowAnimation (float animDuration) {

    }

    public virtual void HideAnimation (float animDuration) {

    }
}